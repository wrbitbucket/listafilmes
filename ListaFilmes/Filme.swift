//
//  Filme.swift
//  ListaFilmes
//
//  Created by Wagner Rodrigues on 22/08/17.
//  Copyright © 2017 Wagner Rodrigues. All rights reserved.
//

//import Foundation
import UIKit

class Filme{
    
    var titulo: String!
    var descricao: String!
    var imagem: UIImage!
    
    init(titulo: String, descricao: String, imagem: UIImage) {
        self.titulo = titulo
        self.descricao = descricao
        self.imagem = imagem
    }
}
