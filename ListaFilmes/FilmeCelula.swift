//
//  FilmeCelula.swift
//  ListaFilmes
//
//  Created by Wagner Rodrigues on 22/08/17.
//  Copyright © 2017 Wagner Rodrigues. All rights reserved.
//

import Foundation
import UIKit

class FilmeCelula: UITableViewCell {
    
    @IBOutlet weak var filmeImageView: UIImageView!
    
    @IBOutlet weak var tituloLabel: UILabel!
    
    @IBOutlet weak var descricaoLabel: UILabel!
    
}
