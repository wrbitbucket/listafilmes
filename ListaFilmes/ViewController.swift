//
//  ViewController.swift
//  ListaFilmes
//
//  Created by Wagner Rodrigues on 22/08/17.
//  Copyright © 2017 Wagner Rodrigues. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {

    var filmes:[Filme] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var filme: Filme
        
        filme = Filme(titulo: "007 - Spectre", descricao: "descricao1", imagem: #imageLiteral(resourceName: "filme1")     )
        filmes.append(filme)
        
        filme = Filme(titulo: "Star Wars", descricao: "descricao2", imagem: #imageLiteral(resourceName: "filme2"))
        filmes.append(filme)
        
        filme = Filme(titulo: "Impacto Mortal", descricao: "descricao3", imagem: #imageLiteral(resourceName: "filme3"))
        filmes.append(filme)
        
        filme = Filme(titulo: "Deadpool", descricao: "descricao4", imagem: #imageLiteral(resourceName: "filme4"))
        filmes.append(filme)
        
        filme = Filme(titulo: "O Regreso", descricao: "descricao5", imagem: #imageLiteral(resourceName: "filme5"))
        filmes.append(filme)
        
        filme = Filme(titulo: "A Herdeira", descricao: "descricao6", imagem: #imageLiteral(resourceName: "filme6"))
        filmes.append(filme)
        
        filme = Filme(titulo: "Caçadores de eomoção", descricao: "descricao7", imagem: #imageLiteral(resourceName: "filme7"))
        filmes.append(filme)
        
        filme = Filme(titulo: "Regresso do mal", descricao: "descricao8", imagem: #imageLiteral(resourceName: "filme8"))
        filmes.append(filme)
        
        filme = Filme(titulo: "Tarzan", descricao: "descricao9", imagem: #imageLiteral(resourceName: "filme9"))
        filmes.append(filme)
        
        filme = Filme(titulo: "Hardcore", descricao: "descricao10", imagem: #imageLiteral(resourceName: "filme10"))
        filmes.append(filme)
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filmes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let filme: Filme = filmes[indexPath.row]
        
        let celulaReuso = "celulaReuso"
        
        let celula = tableView.dequeueReusableCell(withIdentifier: celulaReuso, for: indexPath) as! FilmeCelula
        
        celula.filmeImageView.image = filme.imagem
        celula.tituloLabel.text = filme.titulo
        celula.descricaoLabel.text = filme.descricao
        
        //DEFINIDO EM TEMPO DE EXECUCAO
        //celula.filmeImageView.layer.cornerRadius = 42
        //celula.filmeImageView.clipsToBounds = true
        
        //celula.textLabel?.text = filme.titulo
        //celula.imageView?.image =  filme.imagem
        
        return celula
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "detalheFilme" {
            if let IndexPath = tableView.indexPathForSelectedRow{
                let filmeSelecioando = self.filmes[IndexPath.row]
                let viewControllerDestino = segue.destination as! DetalhesViewController
                
                viewControllerDestino.filme = filmeSelecioando
            }
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

